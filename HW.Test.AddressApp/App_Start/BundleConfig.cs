﻿using System.Web;
using System.Web.Optimization;

namespace HW.Test.AddressApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                        "~/Scripts/angular-file-upload-shim.js",
                        "~/Scripts/angular.js",
                        "~/Scripts/angular-file-upload.js",
                        "~/Scripts/angular-cookies.js",
                        "~/Scripts/angular-resource.js",
                        "~/Scripts/angular-sanitize.js",
                        "~/Scripts/angular-route.js",
                        "~/app/scripts/app.js",
                        "~/app/scripts/controllers/main.js",
                        "~/app/scripts/controllers/upload.js",
                        "~/app/scripts/controllers/search.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/justified-nav.css",
                      "~/Content/jquery.fileupload.css"));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = false;
        }
    }
}
