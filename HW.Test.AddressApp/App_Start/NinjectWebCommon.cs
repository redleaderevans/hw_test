[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(HW.Test.AddressApp.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(HW.Test.AddressApp.App_Start.NinjectWebCommon), "Stop")]

namespace HW.Test.AddressApp.App_Start
{
    using System;
    using System.Web;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject;
    using Ninject.Web.Common;
    using System.Configuration;
    using System.Data.Entity;
    using HW.Test.AddressApp.Domain;
    using HW.Test.AddressApp.Domain.Interfaces;
    using HW.Test.AddressApp.Domain.Concrete;
    using System.Net.Http;
    using Ninject.Activation;
    using System.Collections.Generic;
    using Ninject.Syntax;
    using System.Web.Http.Dependencies;
    using System.Linq;
    using Ninject.Parameters;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
            //System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver = new Ninject.WebApi.DependencyResolver.NinjectDependencyResolver(kernel);

            RegisterServices(kernel);

            System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver = new NinjectResolver(kernel);

            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<ICsvImporter>().To<CsvImporter>().InRequestScope();

            kernel.Bind<DataEntryContext>().ToSelf().InRequestScope().WithConstructorArgument("connection", ConfigurationManager.AppSettings["DataSource"]);

            kernel.Bind<IUkPostcodeProxy>().To<UkPostcodeProxy>().InRequestScope().WithConstructorArgument("endpointUrl", ConfigurationManager.AppSettings["PostcodeServiceRestUrl"]);

            kernel.Bind<HttpClient>().ToSelf().InRequestScope();

            kernel.Bind<IDataEntryRepository>().To<DataEntryRepository>().InRequestScope()
                .WithConstructorArgument("dbContext", context => kernel.Get<DataEntryContext>())
                .WithConstructorArgument("postcodeProxy", context => kernel.Get<UkPostcodeProxy>());

        }
    }

    public class NinjectResolver : NinjectScope, IDependencyResolver
    {
        private readonly IKernel _kernel;
        public NinjectResolver(IKernel kernel)
            : base(kernel)
        {
            _kernel = kernel;
        }
        public IDependencyScope BeginScope()
        {
            return new NinjectScope(_kernel.BeginBlock());
        }
    }

    public class NinjectScope : IDependencyScope
    {
        protected IResolutionRoot resolutionRoot;
        public NinjectScope(IResolutionRoot kernel)
        {
            resolutionRoot = kernel;
        }
        public object GetService(Type serviceType)
        {
            IRequest request = resolutionRoot.CreateRequest(serviceType, null, new Parameter[0], true, true);
            return resolutionRoot.Resolve(request).SingleOrDefault();
        }
        public IEnumerable<object> GetServices(Type serviceType)
        {
            IRequest request = resolutionRoot.CreateRequest(serviceType, null, new Parameter[0], true, true);
            return resolutionRoot.Resolve(request).ToList();
        }
        public void Dispose()
        {
            IDisposable disposable = (IDisposable)resolutionRoot;
            if (disposable != null) disposable.Dispose();
            resolutionRoot = null;
        }
    }
}
