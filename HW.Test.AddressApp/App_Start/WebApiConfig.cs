﻿using HW.Test.AddressApp.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace HW.Test.AddressApp
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //register the global exception handler
            config.Filters.Add(new ApiExceptionFilter());

            // Web API routes
            config.MapHttpAttributeRoutes();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
        }
    }
}
