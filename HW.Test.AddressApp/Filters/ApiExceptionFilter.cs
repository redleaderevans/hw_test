﻿namespace HW.Test.AddressApp.Filters
{
    using CustomExceptions;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Web;
    using System.Web.Http;
    using System.Web.Http.Filters;

    /// <summary>
    /// Global Exception handling filter - registered in ApiConfig
    /// </summary>
    public class ApiExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception is ApiException)
            {
                var error = context.Exception as ApiException;

                //TODO I could extend HttpResponceException to get more control over whats returned including specific error codes and messages
                throw new HttpResponseException(error.HttpCode);
            }
            else
            {
                //catch any other error that occurred
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }
    }
}