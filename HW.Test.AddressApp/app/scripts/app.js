'use strict';

angular.module('postcodeApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'angularFileUpload'
])

//set up the main routes
.config(function ($routeProvider) {
    $routeProvider
    .when('/', {
        templateUrl: 'app/views/home.html',
        controller: 'mainController'
    })
	.when('/upload', {
	    templateUrl: 'app/views/upload.html',
		controller: 'uploadController'
	})
    .when('/search', {
        templateUrl: 'app/views/search.html',
        controller: 'searchController'
    })
    .otherwise({
        redirectTo: '/'
    });
});
