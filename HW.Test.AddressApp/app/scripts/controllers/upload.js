'use strict';

//This should be unit tested with a tool like Jasmine

angular.module('postcodeApp')
    .controller('uploadController', function ($scope, $http, $timeout, $upload) {
        $scope.upload = [];

        //pas ni the files selected
        $scope.onFileSelect = function ($files) {

            for (var i = 0; i < $files.length; i++) {
                var $file = $files[i];
                (function (index) {
                    $scope.upload[index] = $upload.upload({
                        url: "./api/v1.0/upload", // the API url
                        method: "POST",
                        file: $file
                    }).progress(function (evt) {
                        // get upload percentage
                        console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                        //stop any other uploads in the mean time
                        $('#uploadButton').prop('disabled', true);
                        $('.help-block').html('<b>Uploading and processing. Please wait...</b>');
                    }).success(function (data, status, headers, config) {
                        $('#uploadButton').prop('disabled', false);
                        //report
                        $('.help-block').html('<b>Completed sucessfully!</b>');
                        console.log(data);
                    }).error(function (data, status, headers, config) {
                        // file failed to upload
                        console.log(data);
                        $('#uploadButton').prop('disabled', false);
                        //report
                        $('.help-block').html('<b>Ooops something went wrong!</b>');
                    });
                })(i);
            }
        }

        $scope.abortUpload = function (index) {
            $scope.upload[index].abort();
        }
    });

