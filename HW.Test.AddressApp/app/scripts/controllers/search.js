﻿'use strict';

//This should be unit tested with a tool like Jasmine

angular.module('postcodeApp')
    .controller('searchController', function ($scope, $http) {

        $scope.searchPostcode = function () {

            //TODO should do some client side validations here
            var postcode = $('#postcode').val();
            var count = $('#count').val();
            var endpoint = '/api/v1.0/search?postcode=' + postcode + '&count=' + count;

            $http({ method: 'GET', url: endpoint }).
                  success(function (data, status, headers, config) {
                      generateDataTable(data);
                  }).
                  error(function (data, status, headers, config) {
                      generateError();
                  });
        };

        function generateError()
        {
            $('#dataContent').html('');

            $('#dataContent').append('<tr><td colspan=8><b>An error occurred, please ensure the submitted data is valid</b></td></tr>');
        }

        //build a data table row and append it to the results view
        function generateDataTable(data)
        {
            $('#dataContent').html('');

            if (data.length != 0) {
                $.each(data, function (i, dataItem) {
                    var $tr = $('<tr>').append(
                        $('<td>').html('<a href="mailto:' + dataItem.Item.Email + '">' + dataItem.Item.FirstName + ' ' + dataItem.Item.LastName + '</a>'),
                        $('<td>').text(dataItem.Item.Phone),
                        $('<td>').text(dataItem.Item.Mobile),
                        $('<td>').html(dataItem.Item.AddressLineOne + '<br/>' + dataItem.Item.AddressLineTwo + '<br/>' + dataItem.Item.AddressLineThree + '<br/>' + dataItem.Item.Postcode),
                        $('<td>').html('<a href="' + dataItem.Item.Website + '" target="_blank">' + dataItem.Item.BusinessName + '</a>'),
                        $('<td>').text(dataItem.Item.Code),
                        $('<td>').text(dataItem.DistanceInKm)
                    );

                    $('#dataContent').append($tr);
                });
            }
            else
            {
                $('#dataContent').append('<tr><td colspan=8><b>No data found, have you uploaded a CSV yet?</b></td></tr>');
            }
        }

    });