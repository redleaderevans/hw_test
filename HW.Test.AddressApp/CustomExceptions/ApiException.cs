﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace HW.Test.AddressApp.CustomExceptions
{
    /// <summary>
    /// Custom API exception class. 
    /// 
    /// Describes an exception that occurs in the API.
    /// </summary>
    public class ApiException : Exception
    {
        private HttpStatusCode _apiStatusCode;

        public ApiException(HttpStatusCode apiStatusCode = HttpStatusCode.InternalServerError)
        {
            _apiStatusCode = apiStatusCode;

            //TODO extend this with custom erros code, message and maybe reference object
        }

        /// <summary>
        /// The HTTP status code that should be returned
        /// </summary>
        public HttpStatusCode HttpCode
        {
            get { return _apiStatusCode; }
        }

    }
}