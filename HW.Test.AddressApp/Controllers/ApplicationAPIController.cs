﻿namespace HW.Test.AddressApp.Controllers
{
    using CustomExceptions;
    using Domain;
    using Domain.Interfaces;
    using Domain.Models;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Http;

    [RoutePrefix("api/v1.0")]
    public class ApplicationAPIController : ApiController
    {
        private IDataEntryRepository _dataRepo;
        private IUkPostcodeProxy _postcodeProxy;
        private ICsvImporter _csvImporter;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="repo"></param>
        public ApplicationAPIController(IDataEntryRepository repo, IUkPostcodeProxy postcodeProxy, ICsvImporter csvImporter)
        {
            //repo provided by ninject injection
            _dataRepo = repo;
            _postcodeProxy = postcodeProxy;
            _csvImporter = csvImporter;
        }

        /// <summary>
        /// Postcode search method. Returns the number of entitys specified in order of closest to the postcode 
        /// specifed as held in the DB. 
        /// </summary>
        /// <param name="postcode">the postcode you want to search for</param>
        /// <param name="count">the number of items you want returned</param>
        /// <returns></returns>
        [HttpGet, Route("search")]
        public HttpResponseMessage GetNearest([FromUri]string postcode, [FromUri]int count)
        {
            //TODO could offer more filtering options for data returned

            //validate input
            if (string.IsNullOrWhiteSpace(postcode) || count == 0)
            {
                //TODO pass message saying exactly what was wrong
                throw new ApiException(HttpStatusCode.BadRequest);
            }

            //TODO we should check out db first really to see if its been looked up before
            var data = _postcodeProxy.GetData(postcode);

            if(data == null || !string.IsNullOrWhiteSpace(data.Error))
            {
                //invalid postcode
                //TODO pass message saying exactly what was wrong
                throw new ApiException(HttpStatusCode.BadRequest);
            }

           var responseData = _dataRepo.GetNearestToPostcode(data.Geo.Easting, data.Geo.Northing, count);

           return new HttpResponseMessage(HttpStatusCode.OK)
           {
               //TODO I should really be returning a view of the DataEntry not the whole data object itself
               Content = new ObjectContent(typeof(IEnumerable<SearchQueryResult>), responseData, new JsonMediaTypeFormatter())
           };
        }

        /// <summary>
        /// CSV Upload method. 
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("upload")]
        public async Task<HttpResponseMessage> Upload()
        {
            //make sure its a file upload
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new ApiException(HttpStatusCode.BadRequest);
            }

            var provider = new MultipartMemoryStreamProvider();

            await Request.Content.ReadAsMultipartAsync(provider);

            foreach (var file in provider.Contents)
            {
                var filename = file.Headers.ContentDisposition.FileName.Trim('\"');

                //validate its the right file type
                if (!filename.EndsWith("csv", true, CultureInfo.InvariantCulture))
                {
                    throw new ApiException(HttpStatusCode.BadRequest);
                }

                var stream = await file.ReadAsStreamAsync();

                //TODO The CSV import should really be scheduled to run as a background task as it might tak ages. 
                //I could use a DB table to control background tasks and a windows service to 
                //run them in the background. This API method would then respond accepted when the upload is complete 
                //and the task queued.
                //I would probably then expose some methods to get the status of the background task.
                _csvImporter.DoImport(stream, _postcodeProxy, _dataRepo);
            }

            //TODO return the number of items imported
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}