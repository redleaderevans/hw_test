﻿namespace HW.Test.AddressApp.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public class ApplicationUIController : Controller
    {
        //this is a single page angular.js app so we just need to render the index page
        public ActionResult Index()
        {
            return View();
        }
    }
}