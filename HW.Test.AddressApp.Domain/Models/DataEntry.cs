﻿namespace HW.Test.AddressApp.Domain.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Interfaces;

    public class DataEntry : IGeoData
    {
        [Key]
        public int ID { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string AddressLineOne { get; set; }
        public string AddressLineTwo { get; set; }
        public string AddressLineThree { get; set; }
        public string Postcode { get; set; }
        public string BusinessName { get; set; }
        public string Code { get; set; }
        public string Website { get; set; }

        public double Easting { get; set; }
        public double Northing { get; set; }
    }
}
