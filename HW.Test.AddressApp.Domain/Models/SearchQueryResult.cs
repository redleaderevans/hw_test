﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW.Test.AddressApp.Domain.Models
{
    public class SearchQueryResult
    {
        public SearchQueryResult(DataEntry item, double distance)
        {
            Item = item;
            DistanceInMeters = Math.Round(Math.Sqrt(distance));
        }
        public DataEntry Item { get; set; }
        public double DistanceInMeters { get; set; }

        public double DistanceInKm 
        { 
            get
            {
                return (DistanceInMeters / 1000);
            }
        }
    }
}
