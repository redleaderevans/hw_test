﻿namespace HW.Test.AddressApp.Domain.Interfaces
{
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface ICsvImporter
    {
        bool DoImport(Stream fileContentsStream, IUkPostcodeProxy postcodeProxy, IDataEntryRepository dataRepo);

        bool IsValidPostcode(string postcode);
    }
}
