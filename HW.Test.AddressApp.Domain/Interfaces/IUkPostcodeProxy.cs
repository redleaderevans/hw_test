﻿namespace HW.Test.AddressApp.Domain.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Concrete;

    public interface IUkPostcodeProxy
    {
        PostcodeData GetData(string postcode);
    }
}
