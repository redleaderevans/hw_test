﻿namespace HW.Test.AddressApp.Domain.Interfaces
{
    public interface IGeoData
    {
        double Easting { get; set; }
        double Northing { get; set; }
    }
}
