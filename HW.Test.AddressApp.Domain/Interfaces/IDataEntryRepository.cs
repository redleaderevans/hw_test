﻿namespace HW.Test.AddressApp.Domain.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Models;

    public interface IDataEntryRepository : IDisposable
    {
        bool AutoDetectChanges { get; set; }

        DataEntry GetByID(int id);

        List<SearchQueryResult> GetNearestToPostcode(double est, double nor, int count);

        void Add(DataEntry entity);
        void Delete(DataEntry entity);

        void Save();
    }
}
