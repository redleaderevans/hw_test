﻿namespace HW.Test.AddressApp.Domain.Concrete
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Data.Entity;
    using Models;
    using HW.Test.AddressApp.Domain.Interfaces;
    
    public class DataEntryContext :  DbContext
    {
        public DataEntryContext(string connection)
            : base(connection)
        {
        }

        public DataEntryContext()
        {

        }

        //defined as virtual so that mock can override it
        public virtual DbSet<DataEntry> DataEntries { get; set; }
    }
}
