﻿namespace HW.Test.AddressApp.Domain.Concrete
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Interfaces;
    using Models;
    using System.Data.Entity;
    using System.Data.Entity.SqlServer;
    using System.Linq.Expressions;

    public class DataEntryRepository : IDataEntryRepository, IDisposable
    {
        private readonly DataEntryContext _dbContext;

        public DataEntryRepository(DataEntryContext dbContext)
        {
            _dbContext = dbContext;
        }
        /// <summary>
        /// This will speed up inserts when doing the import as it stops EF tracking changes
        /// </summary>
        public bool AutoDetectChanges
        {
            get
            {
                return _dbContext.Configuration.AutoDetectChangesEnabled;
            }
            set
            {
                _dbContext.Configuration.AutoDetectChangesEnabled = value;
            }
        }

        public DataEntry GetFirstByPostCode(string postcode)
        {
            var normalisedPostcode = postcode.Replace(" ", string.Empty).Trim().ToUpper();

            return _dbContext.DataEntries.FirstOrDefault(e => e.Postcode == normalisedPostcode);
        }

        public DataEntry GetByID(int id)
        {
            return _dbContext.DataEntries.SingleOrDefault(e => e.ID == id);
        }

        public List<SearchQueryResult> GetNearestToPostcode(double est, double nor, int count)
        {
            //using SqlCeFunctions.SquareRoot to get the square root here makes this very difficult to unit test
            //so i am excluding it from here for now. The calculation still holds true so no functional difference.

            var data = (from item in _dbContext.DataEntries
                        let distance = (nor - item.Northing) * (nor - item.Northing) + (est - item.Easting) * (est - item.Easting)
                        orderby distance
                        select new { item, distance })  //i believe this will appy in the sql query but would need to profle it to be sure.
                       .Take(count);

            return data.ToList().Select(e => new SearchQueryResult(e.item, e.distance)).ToList();
        }
     
        public void Add(DataEntry entity)
        {
            _dbContext.DataEntries.Add(entity);
        }

        public void Delete(DataEntry entity)
        {
            _dbContext.DataEntries.Remove(entity);
        }

        public void Save()
        {
            _dbContext.SaveChanges(); 
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
