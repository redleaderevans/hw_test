﻿namespace HW.Test.AddressApp.Domain.Concrete
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Simple implementation of a CSV reader
    /// </summary>
    public class CsvReader : StreamReader
    {
        public CsvReader(Stream fileData) : base(fileData) { }

        public List<string> ReadRow()
        {
            List<string> columnValues = null;

            var rowData = ReadLine();

            if(!string.IsNullOrWhiteSpace(rowData))
            {
                columnValues = new List<string>();

                int cursorPosition = 0;
                bool ignoreSeparator = false;
                StringBuilder columnValue = new StringBuilder();

                //need to look at each character in order to split in right place
                while(cursorPosition < rowData.Length)
                {
                    var character = rowData[cursorPosition];

                    switch(character)
                    {
                        case '\x0B':
                            //ignore VT character
                            columnValue.Append(' ');
                            break;
                        case ',':
                            if (!ignoreSeparator)
                            {
                                //get the string value, trim out any starting and ending whitespace
                                columnValues.Add(columnValue.ToString().Trim());

                                //reset current column value
                                columnValue.Clear();
                            }
                            else
                            {
                                columnValue.Append(character);
                            }
                            break;
                        case '"':
                            //if we hit a quotation mark then start ignoring commas when we hit the next pay attention again
                            ignoreSeparator = !ignoreSeparator;
                            break;
                        default:
                            //add to the current column value
                            columnValue.Append(character);
                            break;
                    }

                    cursorPosition++;
                }

                //add the last column value
                columnValues.Add(columnValue.ToString());
            }

            return columnValues;
        }
    }
}
