﻿namespace HW.Test.AddressApp.Domain.Concrete
{
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using HW.Test.AddressApp.Domain.Interfaces;

    /// <summary>
    /// Class to handle communication with the postcode rest api provided by uk-postcodes.com
    /// </summary>
    public class UkPostcodeProxy : IUkPostcodeProxy
    {
        protected readonly string _endpointUrl;
        private HttpClient _httpClient;

        public UkPostcodeProxy(string endpointUrl, HttpClient httpClient)
        {
            _endpointUrl = endpointUrl;
            _httpClient = httpClient;   //nin-jected for testability
        }

        /// <summary>
        /// Returns 
        /// </summary>
        /// <param name="postcode">The postcode to lookup</param>
        /// <returns></returns>
        public PostcodeData GetData(string postcode)
        {
            //fiddler says that the API redirects if you have a lowercase postcode, but we have it uppercase anyway so saving that roundtrip
            var endpoint = _endpointUrl + postcode + ".json";

            var response = _httpClient.GetAsync(endpoint).Result;

            return JsonConvert.DeserializeObject<PostcodeData>(response.Content.ReadAsStringAsync().Result);
        }
    }

    /// <summary>
    /// DTO class for returned data from the postcode api
    /// </summary>
    public class PostcodeData
    {
        //error
        public int Code { get; set; }
        public string Error { get; set; }

        //postcode data
        public string Postcode { get; set; }

        public GeoData Geo { get; set; }

        //TODO add in the other returned types if they are needed
    }

    public class GeoData : IGeoData
    {
        public string GeoHash { get; set; }
        public double Easting { get; set; }
        public double Northing { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
    }
}