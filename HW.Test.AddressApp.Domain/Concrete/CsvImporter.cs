﻿namespace HW.Test.AddressApp.Domain.Concrete
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Interfaces;
    using Models;
    using System.Text.RegularExpressions;

    public class CsvImporter : ICsvImporter
    {
        //regex off tin-ter-net
        private const string _pattern = "(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX‌​]][0-9][A-HJKSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY]))))\\s?[0-9][A-Z-[C‌​IKMOV]]{2})";
        private readonly Regex _regex; 

        public CsvImporter()
        {
            _regex = new Regex(_pattern);
        }

        public bool DoImport(Stream fileContentsStream, IUkPostcodeProxy postcodeProxy, IDataEntryRepository dataRepo)
        {
            //simple caching
            Dictionary<string, IGeoData> _postCodeCache = new Dictionary<string, IGeoData>();

            //this should speed this up doing the inserts
            dataRepo.AutoDetectChanges = false;

            //process file
            using(CsvReader reader = new CsvReader(fileContentsStream))
            {
                var data = reader.ReadRow();

                while (data != null)
                {
                    DataEntry row = new DataEntry
                    {
                        //column 0 is the id which I am assuming we are not bothered by
                        FirstName = data[1],
                        LastName = data[2],
                        Phone = data[3],
                        Mobile = data[4],
                        Email = data[5],
                        AddressLineOne = data[6],
                        AddressLineTwo = data[7],
                        AddressLineThree = data[8],
                        Postcode = data[9].Replace(" ", string.Empty).Trim().ToUpper(), //store in a normalised way
                        BusinessName = data[10],
                        Code = data[11],
                        Website = data[12]
                    };

                    //validate the postcode, only add the row if it is valid
                    if (IsValidPostcode(row.Postcode))
                    {
                        //TODO check DB first to see if we have looked it up previously before going over the wire to the api 
                        //we could even extend the model to include postcode data so we have a perminant cache locally
                        IGeoData postcodeInfo = null;

                        if (_postCodeCache.ContainsKey(row.Postcode))
                        {
                            postcodeInfo = _postCodeCache[row.Postcode];
                        }
                        else
                        {
                            var serverResponse = postcodeProxy.GetData(row.Postcode);

                            if (serverResponse.Code == 0 || string.IsNullOrWhiteSpace(serverResponse.Error))
                            {
                                postcodeInfo = serverResponse.Geo;
                                _postCodeCache.Add(row.Postcode, postcodeInfo);
                            }
                        }

                        //if no error is returned
                        if(postcodeInfo != null)
                        {
                            row.Northing = postcodeInfo.Northing;
                            row.Easting = postcodeInfo.Easting;

                            //TODO should add some duplicate checking here but for now I will just ignore it
                            dataRepo.Add(row);

                        }
                    }

                    //continue reading rows
                    data = reader.ReadRow();
                }

                //save all items added
                dataRepo.Save();
            }

            return true;
        }

        public bool IsValidPostcode(string postcode)
        {
            var match = _regex.Match(postcode);
            
            return match.Success;
        }
    }
}
