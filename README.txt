CSV UK address importer and nearest address calculator.
--------------------------------------------------------------

This is a VS2013 solution. Just get source, do nuget 
package restore and run with HW.Test.AddressApp as startup

DETAILS
--------------------------------------------------------------
Backend : .Net 4.5, EF 6 with a SQLCE 4 embedded DB
Frontend: AngularJS Website + WebAPI 2 RESTful API

[POST] /api/v1.0/upload - 500-uk.csv
[GET] /api/v1.0/search?postcode=rg48qn&count=10

NOTES
--------------------------------------------------------------
This is a fully working solution but is lacking lots of 
polish... 
There are some areas of code tidy required and areas 
that enhancements would be made if this were to be a 
production solution. These have mostly 
been highlighted by TODO items. The DB is SQL CE but 
could easily be swaped out for any other provider.