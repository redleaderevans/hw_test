﻿using HW.Test.AddressApp.Domain.Concrete;
using HW.Test.AddressApp.Domain.Interfaces;
using HW.Test.AddressApp.Domain.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HW.Test.AddressApp.Domain.Tests
{
    [TestClass]
    public class CsvImporterTests
    {
        private UkPostcodeProxy _proxy;
        private Mock<DataEntryContext> _mockContext;
        private Mock<DataEntryRepository> _dataRepo;

        public CsvImporterTests()
        {
            //mock the http response to the postcode lookup proxy
            string endpoint = "http://uk-postcodes.com/postcode/";
            var responseMessage = new HttpResponseMessage();
            responseMessage.Content = new StringContent("{\"postcode\":\"B16 8AE\",\"geo\":{\"lat\":52.47933472657798,\"lng\":-1.917546897645123,\"easting\":405696.0,\"northing\":286826.0,\"geohash\":\"http://geohash.org/gcqds3edmcj3\"},\"administrative\":{\"council\":{\"title\":\"Birmingham\",\"uri\":\"http://statistics.data.gov.uk/id/statistical-geography/E08000025\",\"code\":\"E08000025\"},\"ward\":{\"title\":\"Ladywood\",\"uri\":\"http://statistics.data.gov.uk/id/statistical-geography/E05001193\",\"code\":\"E05001193\"},\"constituency\":{\"title\":\"Birmingham, Ladywood\",\"uri\":\"http://statistics.data.gov.uk/id/statistical-geography/E14000564\",\"code\":\"E14000564\"}}}");

            var messageHandler = new FakeHttpMessageHandler(responseMessage);
            var mockHttpClient = new HttpClient(messageHandler);

            //create a mocked proxy
            _proxy = new UkPostcodeProxy(endpoint, mockHttpClient);

            //mock out db set so we can fake adding entitys
            var mockSet = new Mock<DbSet<DataEntry>>();

            //mock the datbase context
            _mockContext = new Mock<DataEntryContext>();
            _mockContext.Setup(m => m.DataEntries).Returns(mockSet.Object);

            //lastly mock up a data repo instance that uses the above mocked data context
            _dataRepo = new Mock<DataEntryRepository>(_mockContext.Object);
        }

        [TestMethod]
        public void CsvImporter_DoImport_Test1()
        {
            //pull the file from the project folder
            string filePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Resources\500-uk.csv");

            using (FileStream fileStream = File.OpenRead(filePath))
            {
                //read into memory stream
                MemoryStream memStream = new MemoryStream();
                memStream.SetLength(fileStream.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);

                //crate an instance of the CSV importer
                CsvImporter importer = new CsvImporter();

                //finally try and do the import
                Assert.IsTrue(importer.DoImport(memStream, _proxy, _dataRepo.Object));
            }
        }

        [TestMethod]
        public void CsvImporter_IsValidPostCode_Valid()
        {
            //simple test of regex function to validate postcode
            CsvImporter importer = new CsvImporter();

            Assert.IsTrue(importer.IsValidPostcode("RG4 8QN"));
            Assert.IsTrue(importer.IsValidPostcode("RG30 4QA"));
            Assert.IsTrue(importer.IsValidPostcode("SA459TU"));
        }

        [TestMethod]
        public void CsvImporter_IsValidPostCode_Invalid()
        {
            CsvImporter importer = new CsvImporter();

            Assert.IsFalse(importer.IsValidPostcode("4 8QN"));
            Assert.IsFalse(importer.IsValidPostcode("RG30 77QA"));
            Assert.IsFalse(importer.IsValidPostcode("SA45|9TU"));
        }
    }
}
