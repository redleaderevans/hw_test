﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HW.Test.AddressApp.Domain.Interfaces;
using HW.Test.AddressApp.Domain.Models;
using Moq;
using System.Data.Entity;
using HW.Test.AddressApp.Domain.Concrete;
using System.Linq;
using System.Data.Entity.SqlServerCompact;

namespace HW.Test.AddressApp.Domain.Tests
{
    /// <summary>
    /// Summary description for DataEntryRepositoryTests
    /// </summary>
    [TestClass]
    public class DataEntryRepositoryTests
    {
        private Mock<DataEntryContext> _mockContext;

        public DataEntryRepositoryTests()
        {
            // create some mock data to use 
            var entries = new List<DataEntry>
                {
                    new DataEntry 
                    { 
                        ID = 1,
                        Easting = 471139,
                        Northing = 176751,
                        Postcode = "RG48QN"
                    },
                    new DataEntry 
                    { 
                        ID = 2,
                        Easting = 403894,
                        Northing = 278939,
                        Postcode = "B388AJ" //B38 8AJ	403894	278939	
                    },
                    new DataEntry 
                    { 
                        ID = 3,
                        Easting = 454698,
                        Northing = 246855,
                        Postcode = "OX172BJ" //OX17 2BJ	454698	246855	
                    },
                    new DataEntry 
                    { 
                        ID = 4,
                        Easting = 531459,
                        Northing = 186717,
                        Postcode = "N42DN" // N4 2DN	531459	186717	
                    },
                    
                }.AsQueryable();

            //mock up a db set to use , using the above data
            var mockSet = new Mock<DbSet<DataEntry>>();
            mockSet.As<IQueryable<DataEntry>>().Setup(m => m.Provider).Returns(entries.Provider);
            mockSet.As<IQueryable<DataEntry>>().Setup(m => m.Expression).Returns(entries.Expression);
            mockSet.As<IQueryable<DataEntry>>().Setup(m => m.ElementType).Returns(entries.ElementType);
            mockSet.As<IQueryable<DataEntry>>().Setup(m => m.GetEnumerator()).Returns(entries.GetEnumerator()); 

            //get a db context
            _mockContext = new Mock<DataEntryContext>();

            //join our data to the mocked context
            _mockContext.Setup(m => m.DataEntries).Returns(mockSet.Object); 
        }

        [TestMethod]
        public void DataEntryRepository_GetNearestToPostcode()
        {
            //mock the repo
            DataEntryRepository repo = new DataEntryRepository(_mockContext.Object);

            //test the main function
            var data = repo.GetNearestToPostcode(471139, 176751, 4);

            Assert.IsNotNull(data);
            Assert.IsInstanceOfType(data, typeof(List<SearchQueryResult>));
            Assert.AreEqual(data.Count, 4);

            //test coverage to ensure the calculation is correct and returning the items in the right order
            Assert.AreEqual(data[0].Item.ID, 1);
            Assert.AreEqual(data[1].Item.ID, 4);
            Assert.AreEqual(data[2].Item.ID, 3);
            Assert.AreEqual(data[3].Item.ID, 2);

            Assert.AreEqual(data[0].DistanceInMeters, 0);
            Assert.AreEqual(data[1].DistanceInMeters, 61138);
            Assert.AreEqual(data[2].DistanceInMeters, 72006);
            Assert.AreEqual(data[3].DistanceInMeters, 122329);
        }

        [TestMethod]
        public void DataEntryRepository_GetNearestToPostcode_AskForMoreThanThereAre()
        {
            //mock the repo
            DataEntryRepository repo = new DataEntryRepository(_mockContext.Object);

            var data = repo.GetNearestToPostcode(471139, 176472, 100);

            Assert.IsNotNull(data);
            Assert.IsInstanceOfType(data, typeof(List<SearchQueryResult>));
            Assert.AreEqual(data.Count, 4);
        }

        [TestMethod]
        public void DataEntryRepository_GetByID_ValidID()
        {
            //mock the repo
            DataEntryRepository repo = new DataEntryRepository(_mockContext.Object);

            var data = repo.GetByID(4);

            Assert.IsNotNull(data);
            Assert.IsInstanceOfType(data, typeof(DataEntry));
            Assert.AreEqual(data.ID, 4);
        }

        [TestMethod]
        public void DataEntryRepository_GetByID_InvalidID()
        {
            //mock the repo
            DataEntryRepository repo = new DataEntryRepository(_mockContext.Object);

            //99 doesnt exist in out data
            var data = repo.GetByID(99);

            Assert.IsNull(data);
        }
    }
}
