﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Collections.Generic;
using System.Text;
using HW.Test.AddressApp.Domain.Concrete;

namespace HW.Test.AddressApp.Domain.Tests
{
    /// <summary>
    /// Test coverage of the simple CSV reader.
    /// 
    /// Fakes an input stream to the reader then validates outputs.
    /// </summary>
    [TestClass]
    public class CsvReaderTests
    {
        [TestMethod]
        public void CsvReader_ReadRow_SingleRow()
        {
            string input = "value1,value2,value3\r\n";

            using (var dataStream = GenerateStream(input))
            {
                using (var reader = new CsvReader(dataStream))
                {
                    List<string> columnData = reader.ReadRow();

                    Assert.AreEqual(columnData.Count, 3);
                    Assert.AreEqual(columnData[0], "value1");
                    Assert.AreEqual(columnData[1], "value2");
                    Assert.AreEqual(columnData[2], "value3");
                }
            }
        }

        [TestMethod]
        public void CsvReader_ReadRow_SingleRow_Spaces()
        {
            string input = "value 1 , test value 2,another test value 4\r\n";

            using (var dataStream = GenerateStream(input))
            {
                using (var reader = new CsvReader(dataStream))
                {
                    List<string> columnData = reader.ReadRow();

                    Assert.AreEqual(columnData.Count, 3);
                    Assert.AreEqual(columnData[0], "value 1");
                    Assert.AreEqual(columnData[1], "test value 2");
                    Assert.AreEqual(columnData[2], "another test value 4");
                }
            }
        }

        [TestMethod]
        public void CsvReader_ReadRow_SingleRow_EmptyColumn()
        {
            string input = "value 1 , test value 2,another test value 4,\r\n";

            using (var dataStream = GenerateStream(input))
            {
                using (var reader = new CsvReader(dataStream))
                {
                    List<string> columnData = reader.ReadRow();

                    Assert.AreEqual(columnData.Count, 4);
                    Assert.AreEqual(columnData[0], "value 1");
                    Assert.AreEqual(columnData[1], "test value 2");
                    Assert.AreEqual(columnData[2], "another test value 4");
                    Assert.AreEqual(columnData[3], string.Empty);
                }
            }
        }

        [TestMethod]
        public void CsvReader_ReadRow_SingleRow_EmptyQuotedColumn()
        {
            string input = "value 1 , test value 2,\"\",something else\r\n";

            using (var dataStream = GenerateStream(input))
            {
                using (var reader = new CsvReader(dataStream))
                {
                    List<string> columnData = reader.ReadRow();

                    Assert.AreEqual(columnData.Count, 4);
                    Assert.AreEqual(columnData[0], "value 1");
                    Assert.AreEqual(columnData[1], "test value 2");
                    Assert.AreEqual(columnData[2], string.Empty);
                    Assert.AreEqual(columnData[3], "something else");
                }
            }
        }

        [TestMethod]
        public void CsvReader_ReadRow_SingleRow_AllEmptyColumns()
        {
            string input = ",,,,,\r\n";

            using (var dataStream = GenerateStream(input))
            {
                using (var reader = new CsvReader(dataStream))
                {
                    List<string> columnData = reader.ReadRow();

                    Assert.AreEqual(columnData.Count, 6);
                    Assert.AreEqual(columnData[0], string.Empty);
                    Assert.AreEqual(columnData[1], string.Empty);
                    Assert.AreEqual(columnData[2], string.Empty);
                    Assert.AreEqual(columnData[3], string.Empty);
                    Assert.AreEqual(columnData[4], string.Empty);
                    Assert.AreEqual(columnData[5], string.Empty);
                }
            }
        }

        [TestMethod]
        public void CsvReader_ReadRow_SingleRow_QuotedValue()
        {
            string input = "value1,\"value2\",value3\r\n";

            using (var dataStream = GenerateStream(input))
            {
                using (var reader = new CsvReader(dataStream))
                {
                    List<string> columnData = reader.ReadRow();

                    Assert.AreEqual(columnData.Count, 3);
                    Assert.AreEqual(columnData[0], "value1");
                    Assert.AreEqual(columnData[1], "value2");
                    Assert.AreEqual(columnData[2], "value3");
                }
            }
        }

        [TestMethod]
        public void CsvReader_ReadRow_EmptyRow()
        {
            string input = "\r\n";

            using (var dataStream = GenerateStream(input))
            {
                using (var reader = new CsvReader(dataStream))
                {
                    List<string> columnData = reader.ReadRow();

                    Assert.AreEqual(columnData, null);
                }
            }
        }

        [TestMethod]
        public void CsvReader_ReadRow_SingleRow_Symbols()
        {
            string input = "Chelwood Wing \x0B Chamberlain Hall,!£$%^&*(),!£$%^&*()\r\n";

            using (var dataStream = GenerateStream(input))
            {
                using (var reader = new CsvReader(dataStream))
                {
                    List<string> columnData = reader.ReadRow();

                    Assert.AreEqual(columnData.Count, 3);
                    Assert.AreEqual(columnData[0], "Chelwood Wing   Chamberlain Hall");
                    Assert.AreEqual(columnData[1], "!?$%^&*()");
                    Assert.AreEqual(columnData[2], "!?$%^&*()");
                }
            }
        }

        private Stream GenerateStream(string value)
        {
            //read the input string into a memory stream
            var bytes = Encoding.ASCII.GetBytes(value);

            MemoryStream stream = new MemoryStream(bytes);
            stream.Position = 0; //just check the stream position is correct

            return stream;
        }
    }
}
