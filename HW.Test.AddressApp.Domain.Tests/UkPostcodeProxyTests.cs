﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using HW.Test.AddressApp.Domain.Interfaces;
using HW.Test.AddressApp.Domain.Concrete;

namespace HW.Test.AddressApp.Domain.Tests
{
    /// <summary>
    /// Unit testing coverage for UkPostcodeProxy
    /// 
    /// The following tests use a fake HttpMessageandler to simulate the response rather than gactually going over the wire.
    /// Then validates that the Json deserialize was successful.
    /// </summary>
    [TestClass]
    public class UkPostcodeProxyTests
    {
        [TestMethod]
        public void UkPostcodeProxy_GetData_MockedHttpClient_ValidPostcode()
        {
            //fake the valid http response with details
            string endpoint = "http://uk-postcodes.com/postcode/";
            var responseMessage = new HttpResponseMessage();
            responseMessage.Content = new StringContent("{\"postcode\":\"B16 8AE\",\"geo\":{\"lat\":52.47933472657798,\"lng\":-1.917546897645123,\"easting\":405696.0,\"northing\":286826.0,\"geohash\":\"http://geohash.org/gcqds3edmcj3\"},\"administrative\":{\"council\":{\"title\":\"Birmingham\",\"uri\":\"http://statistics.data.gov.uk/id/statistical-geography/E08000025\",\"code\":\"E08000025\"},\"ward\":{\"title\":\"Ladywood\",\"uri\":\"http://statistics.data.gov.uk/id/statistical-geography/E05001193\",\"code\":\"E05001193\"},\"constituency\":{\"title\":\"Birmingham, Ladywood\",\"uri\":\"http://statistics.data.gov.uk/id/statistical-geography/E14000564\",\"code\":\"E14000564\"}}}");

            var messageHandler = new FakeHttpMessageHandler(responseMessage);
            var mockHttpClient = new HttpClient(messageHandler);
 
            //create a mocked proxy instance
            var proxy = new UkPostcodeProxy(endpoint, mockHttpClient);

            var response = proxy.GetData("B16 8AE");

            Assert.IsNotNull(response);
            Assert.IsNull(response.Error);
            Assert.AreEqual(response.Code, 0);
            Assert.AreEqual(response.Postcode, "B16 8AE");
            Assert.AreEqual(response.Geo.Easting, 405696);
            Assert.AreEqual(response.Geo.Northing, 286826);
        }

        [TestMethod]
        public void UkPostcodeProxy_GetData_MockedHttpClient_InvalidPostcode()
        {
            //fake a error state response
            string endpoint = "http://uk-postcodes.com/postcode/";
            var responseMessage = new HttpResponseMessage();
            responseMessage.Content = new StringContent("{\"code\":404,\"error\":\"Postcode RG4777 is not valid\"}");

            var messageHandler = new FakeHttpMessageHandler(responseMessage);
            var mockHttpClient = new HttpClient(messageHandler);

            var proxy = new UkPostcodeProxy(endpoint, mockHttpClient);

            var response = proxy.GetData("RG4777");

            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Error);
            Assert.AreEqual(response.Code, 404);
            Assert.IsNull(response.Postcode);
            Assert.IsNull(response.Geo);
        }
    }

    /// <summary>
    /// Helper class to allow us to fake the response rather than actually go over the wire
    /// </summary>
    public class FakeHttpMessageHandler : HttpMessageHandler
    {
        HttpResponseMessage response;

        public FakeHttpMessageHandler(HttpResponseMessage response)
        {
            this.response = response;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
                                                System.Threading.CancellationToken cancellationToken)
        {
            var tcs = new TaskCompletionSource<HttpResponseMessage>();

            tcs.SetResult(response);

            return tcs.Task;
        }
    }
}
